/**
 * 
 */
function like(){
	$(".like").click(function(){
		var postId = this.id;
		var postUserId = getLastItem(window.location.pathname); // Returns path
		var aUserId = getCookie("id");
		
		postlike(postId, postUserId, aUserId);
		
    $('#' + postId).toggleClass("btn-success");	
});
}

function follow(){
	$("#follow").click(function(){
		var followee = getLastItem(window.location.pathname);
		var follower = getCookie("id");
		console.log(follower + ' ' + followee);
		postFollower(follower, followee);
	    $('#follow').toggleClass("btn-success");
	    
	    //Change inner HTML based on CSS Class
	    if($('#follow').hasClass('btn-success')) {
	    	  console.log("has class btn-success");
	    	  $("#follow").html("unfollow");
	    	}
	    	else {
	    		console.log("else statement");
	    		$("#follow").html("follow");
	    	}
		
	});
	
}
	
	
	// gets the cookie with the given name
	function getCookie(cname) {
		  var name = cname + "=";
		  var decodedCookie = decodeURIComponent(document.cookie);
		  var ca = decodedCookie.split(';');
		  for(var i = 0; i <ca.length; i++) {
		    var c = ca[i];
		    while (c.charAt(0) == ' ') {
		      c = c.substring(1);
		    }
		    if (c.indexOf(name) == 0) {
		      return c.substring(name.length, c.length);
		    }
		  }
		  return "";
		}
	
	// gets the last subString after char "/"
	function getLastItem (thePath){ 
		return thePath.substring(thePath.lastIndexOf('/') + 1);
	}
	
	// Post the like
	function postlike(postId, postUserId, aUserId)
	{
		$.post('/post/like',
							JSON.stringify({
									postId : postId,
									userPosted : postUserId,
									userLiked : aUserId
								}),
							null,
							'json');
	}
	
	// Post the follower
	function postFollower(follower, followee)
	{
		$.post('/follow',
							JSON.stringify({
									followerId : follower,
									followeeId : followee
								}),
							null,
							'json');
	}
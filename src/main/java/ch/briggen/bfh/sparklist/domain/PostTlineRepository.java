package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Posts mit User Context. 
 * 
 * @author Tim Weinekötter
 *
 */
public class PostTlineRepository {

	private final Logger log = LoggerFactory.getLogger(PostTlineRepository.class);

	/**
	 * Liefert alle Posts in der Datenbank
	 * 
	 * @return Collection aller PostsTline
	 */
	public Collection<PostTline> getAll() {
		log.trace("getAll Posts with User Context");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select * from USERS_POSTS_CONTEXT ORDER BY POST_DATETIME DESC");
			ResultSet rs = stmt.executeQuery();
			return mapPostsTline(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all post with user context";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	/**
	 * Liefert alle Post mit übergebner user_Id
	 * 
	 * @param Userid
	 * @return Collection of posts mit User Context
	 */
	public Collection<PostTline> getByUserId(long id) {
		log.trace("getAll by UserId");
		return null;
	}
	/**
	 * maped die übbergebener Posts mit User Context in rs in eine Collection
	 * 
	 * @param ResultSet
	 * @return Collectio<PostTline>
	 */
	private Collection<PostTline> mapPostsTline(ResultSet rs) throws SQLException {
		LinkedList<PostTline> list = new LinkedList<PostTline>();
		while (rs.next()) {
			PostTline p = new PostTline(
					rs.getLong("user_id"), 
					rs.getString("first_name"), 
					rs.getString("last_name"), 
					rs.getLong("post_id"),
					rs.getString("summary"),
					rs.getString("content"),
					rs.getTimestamp("post_datetime"));
			list.add(p);
		}
		return list;

	}

}

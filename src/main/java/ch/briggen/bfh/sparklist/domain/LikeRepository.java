package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Likes. Hier werden alle Funktionen für die DB-Operationen
 * zu Likes implementiert
 * 
 * @author Tim Weinekötter
 *
 */

public class LikeRepository {

	private final Logger log = LoggerFactory.getLogger(LikeRepository.class);

	/**
	 * Überprüft ob es bereits eine Like Instanz in der DB gibt. SELECT.
	 * 
	 * @param l
	 * @return boolean
	 */
	public boolean getLike(Like l) {
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select POST_ID, USER_LIKED, USER_POSTED FROM LIKES where POST_ID=? AND USER_LIKED=? AND USER_POSTED=?");
			stmt.setLong(1, l.getPostId());
			stmt.setLong(2, l.getUserLiked());
			stmt.setLong(3, l.getUserPosted());
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				// rs is empty
				return true;
			}
			return false;

		} catch (SQLException e) {
			String msg = "SQL error while getting like " + l;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * speichert ein Like in der Datenbank. INSERT.
	 * 
	 * @param l
	 * 
	 */
	public void like(Like l) {
		log.trace("insert " + l);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("insert into likes (POST_ID, USER_LIKED, USER_POSTED) values (?,?,?)");
			stmt.setLong(1, l.getPostId());
			stmt.setLong(2, l.getUserLiked());
			stmt.setLong(3, l.getUserPosted());
			System.out.println("ttest" + stmt);
			stmt.execute();

		} catch (SQLException e) {
			String msg = "SQL error while inserting like " + l;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * löscht ein Like in der Datenbank. DELETE.
	 * 
	 * @param l
	 * 
	 */
	public void unlike(Like l) {
		log.trace("delete " + l);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("delete from likes where post_id =?");
			stmt.setLong(1, l.getPostId());
			stmt.execute();

		} catch (SQLException e) {
			String msg = "SQL error while deleting like " + l;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * gibt alle Likes mit übergebenr id zurück
	 * 
	 * @param id
	 * @return Collectio<like>
	 */
	public Collection<Like> getLikes(long id) {
		log.trace("get likes from user with id:  " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select POST_ID, USER_LIKED, USER_POSTED FROM LIKES where USER_POSTED=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapLikes(rs);
		} catch (SQLException e) {
			String msg = "SQL error while getting likes of user with id:  " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * maped die übbergebener Likes in rs in eine Collection
	 * 
	 * @param ResultSet
	 * @return Collectio<like>
	 */
	private Collection<Like> mapLikes(ResultSet rs) throws SQLException {
		LinkedList<Like> list = new LinkedList<Like>();
		while (rs.next()) {
			Like u = new Like(rs.getLong("Post_Id"), rs.getLong("user_liked"), rs.getLong("user_posted"));
			list.add(u);
		}
		return list;
	}

}

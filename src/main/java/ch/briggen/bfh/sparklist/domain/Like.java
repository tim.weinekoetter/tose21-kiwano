package ch.briggen.bfh.sparklist.domain;

/**
 * Like Klasse mit jeweils zwei userIds (userLiked und userPosted), postId und
 * eine eindeutige Id für die Like Instanz
 * 
 * @author Tim Weinekötter
 *
 */
public class Like {

	private long id;
	private long postId;
	private long userLiked;
	private long userPosted;

	public Like(long postId, long userLiked, long userPosted) {
		super();
		this.postId = postId;
		this.userLiked = userLiked;
		this.userPosted = userPosted;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPostId() {
		return postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	public long getUserLiked() {
		return userLiked;
	}

	public void setUserLiked(long userLiked) {
		this.userLiked = userLiked;
	}

	public long getUserPosted() {
		return userPosted;
	}

	public void setUserPosted(long userPosted) {
		this.userPosted = userPosted;
	}

	@Override
	public String toString() {
		return "Like [postId=" + postId + ", userLiked=" + userLiked + ", userPosted=" + userPosted + "]";
	}

}

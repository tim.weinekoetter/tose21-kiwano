package ch.briggen.bfh.sparklist.domain;

import java.text.SimpleDateFormat;

/**
 * Post Klasse mit userId, summary des Posts, Content des Posts, Timestamp des Posts und eine eindeutige ID
 * eine eindeutige Id für die Like Instanz
 * 
 * @author Tim Weinekötter
 *
 */
public class Post {

	private long id;
	private long user_id;
	private String summary;
	private String content;
	private java.sql.Timestamp ins_datetime;



	public Post() {

	}

	public Post(long user_id, String summary, String content) {
		super();
		this.user_id = user_id;
		this.summary = summary;
		this.content = content;
	}

	public Post(long id, long user_id, String summary, String content, java.sql.Timestamp ins_datetime) {
		this.id = id;
		this.user_id = user_id;
		this.summary = summary;
		this.content = content;
		this.ins_datetime = ins_datetime;
	}
	

	public String getDate(){ 
	    SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy h:mm a");  
	    String strDate= formatter.format(ins_datetime);  
	    System.out.println(ins_datetime);  
		return strDate;
				
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public java.sql.Timestamp getIns_datetime() {
		return ins_datetime;
	}

	public void setIns_datetime(java.sql.Timestamp ins_datetime) {
		this.ins_datetime = ins_datetime;
	}

}

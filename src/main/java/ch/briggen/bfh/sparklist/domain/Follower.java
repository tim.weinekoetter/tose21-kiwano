package ch.briggen.bfh.sparklist.domain;

public class Follower {
	private long followerId;
	private long followeeId;

	public Follower(long followerId, long followeeId) {
		super();
		this.followerId = followerId;
		this.followeeId = followeeId;
	}

	public Follower() {
		super();
	}

	public long getFollowerId() {
		return followerId;
	}

	public void setFollowerId(long followerId) {
		this.followerId = followerId;
	}

	public long getFolloweeId() {
		return followeeId;
	}

	public void setFolloweeId(long followeeId) {
		this.followeeId = followeeId;
	}

}

package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle User Operationen
 * 
 * 
 * @author Tim Weinekötter
 *
 */
public class UserRepository {

	private final Logger log = LoggerFactory.getLogger(UserRepository.class);

	/**
	 * liefert alle Users in der DB
	 * 
	 * 
	 * @return Collectio<User>
	 */
	public Collection<User> getAll() {
		log.trace("getAll");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select ID, FIRST_NAME, LAST_NAME, BIO, EMAIL, PW from USERS");
			ResultSet rs = stmt.executeQuery();
			return mapUsers(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * liefert den User mit übergebener id
	 * 
	 * @param id
	 * @return User
	 */
	public User getById(long id) {

		log.trace("getById " + id);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select id, first_name, last_name, bio, email, pw from users where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapUsers(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * gibt die userId mit übergebener email zurück
	 * 
	 * @param email
	 * @return id
	 */
	public Long getIdByEmail(String email) {
		log.trace("getByIdBy " + email);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select id, email, from users where email=?");
			stmt.setString(1, email);
			ResultSet rs = stmt.executeQuery();
			// checkt ob result set leer ist
			if (!rs.next()) {
				return null;
			}
			return rs.getLong("id");

		} catch (SQLException e) {
			String msg = "SQL error while retreiving id by email " + email;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	private Collection<User> mapUsers(ResultSet rs) throws SQLException {
		LinkedList<User> list = new LinkedList<User>();
		while (rs.next()) {
			User u = new User(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"),
					rs.getString("bio"), rs.getString("email"), rs.getString("pw"));
			list.add(u);
		}
		return list;
	}

	/**
	 * validiert ob email und pw korrekt sind
	 * 
	 * @param email, pw
	 * @return boolean
	 */
	public boolean validate(String email, String pw) {
		log.trace("validate");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement("select EMAIL, PW from USERS WHERE EMAIL=? AND PW =?");
			stmt.setString(1, email);
			stmt.setString(2, pw);
			System.out.println(stmt);

			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				System.out.println("failed");
				return false;
			} else {
				System.out.println("success");
				return true;
			}
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * gibt den User mit übergebener email und pw zurück
	 * 
	 * @param email, pw
	 * @return User
	 */
	public User getByEmailPW(String email, String pw) {
		log.trace("getByEmailPW");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select ID, FIRST_NAME, LAST_NAME, BIO, EMAIL, PW from USERS where email=? and pw=?");
			stmt.setString(1, email);
			stmt.setString(2, pw);
			ResultSet rs = stmt.executeQuery();
			return mapUsers(rs).iterator().next();
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * erstellt ein neuen User in der DB
	 * 
	 * @param User
	 * @return id
	 */
	public long insert(User u) {
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"insert into users (first_name, last_name, bio, email, pw) values (?,?,?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, u.getFirstName());
			stmt.setString(2, u.getLastName());
			stmt.setString(3, u.getBio());
			stmt.setString(4, u.getEmail());
			stmt.setString(5, u.getPw());
			System.out.println(stmt);
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		} catch (SQLException e) {
			String msg = "SQL error while inserting user " + u;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

	/**
	 * aktualisert die Benutzerangaben firstname, lastname, bio
	 * 
	 * @param User
	 * 
	 */
	public void save(User u) {
		log.trace("save " + u);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("update users set FIRST_NAME=?, LAST_NAME=?, BIO=? where ID=?");
			stmt.setString(1, u.getFirstName());
			stmt.setString(2, u.getLastName());
			stmt.setString(3, u.getBio());
			stmt.setLong(4, u.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			String msg = "SQL error while updating user " + u;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

}

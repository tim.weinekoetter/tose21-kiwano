package ch.briggen.bfh.sparklist.domain;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * PostTline Klasse Post mit User Angaben
 * 
 * @author Tim Weinekötter
 */
public class PostTline {
	private long userId;
	private String firstname;
	private String lastname;
	private long postId;
	private String summary;
	private String content;
	private java.sql.Timestamp insDatetime;

	public PostTline(long userId, String firstname, String lastnem, long postId, String summary, String content,
			Timestamp insDatetime) {
		super();
		this.userId = userId;
		this.firstname = firstname;
		this.lastname = lastnem;
		this.postId = postId;
		this.summary = summary;
		this.content = content;
		this.insDatetime = insDatetime;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastnem) {
		this.lastname = lastnem;
	}

	public long getPostId() {
		return postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getInsDatetime() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy h:mm a");  
	    String strDate= formatter.format(insDatetime);  
	    System.out.println(insDatetime);  
		return strDate;
	}

	public void setInsDatetime(java.sql.Timestamp insDatetime) {
		this.insDatetime = insDatetime;
	}

	@Override
	public String toString() {
		return "PostTline [userId=" + userId + ", firstname=" + firstname + ", lastname=" + lastname + ", postId="
				+ postId + ", summary=" + summary + ", content=" + content + ", insDatetime=" + insDatetime + "]";
	}

}

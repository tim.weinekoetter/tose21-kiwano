package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle Posts. Hier werden alle Funktionen für die DB-Operationen
 * zu Posts implementiert
 * 
 * @author Tim Weinekötter
 *
 */
public class PostRepository {

	private final Logger log = LoggerFactory.getLogger(PostRepository.class);

	/**
	 * Liefert alle Posts in der Datenbank
	 * 
	 * @return Collection aller Posts
	 */
	public Collection<Post> getAll() {
		// TODO
		return null;
	}

	/**
	 * Liefert alle Post mit übergebner user_Id
	 * 
	 * @param Userid
	 * @return Collection of posts oder null
	 */
	public Collection<Post> getByUserId(long id) {
		log.trace("getAll by UserId");
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("select id, user_id, summary, content, ins_datetime from posts where user_id=? order by ins_datetime desc");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapPosts(rs);
		} catch (SQLException e) {
			String msg = "SQL error while retreiving all post with userId: " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	/**
	 * maped die übbergebener Posts in rs in eine Collection
	 * 
	 * @param ResultSet
	 * @return Collectio<Post>
	 */
	private Collection<Post> mapPosts(ResultSet rs) throws SQLException {
		LinkedList<Post> list = new LinkedList<Post>();
		while (rs.next()) {
			Post p = new Post(rs.getLong("id"), rs.getLong("user_id"), rs.getString("summary"), rs.getString("content"),
					rs.getTimestamp("ins_datetime"));
			list.add(p);
		}
		return list;
	}

	/**
	 * Erstellt einen neuen Post
	 * 
	 * @param Post
	 */
	public void insert(Post p) {
		log.trace("insert " + p);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("INSERT INTO POSTS  (user_ID ,summary ,content ) VALUES (?,?,?)");
			stmt.setLong(1, p.getUser_id());
			stmt.setString(2, p.getSummary());
			stmt.setString(3, p.getContent());
			stmt.execute();
		} catch (SQLException e) {
			String msg = "SQL error while updating item " + p;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}

	}

}

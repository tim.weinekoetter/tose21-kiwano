package ch.briggen.bfh.sparklist.domain;

/**
 * User Klasse mit id, firstname, lastname, bio, email und pw
 *
 * 
 * @author Tim Weinekötter
 *
 */
public class User {

	private long id;
	private String firstName;
	private String lastName;
	private String bio;
	private String email;
	private String pw;

	public User(long id, String firstName, String lastName, String bio, String email, String pw) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.bio = bio;
		this.email = email;
		this.pw = pw;
	}

	public User(String email, String pw) {
		this.email = email;
		this.pw = pw;
	}

	public User() {

	}

	public User(long id, String firstName, String lastName, String bio) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.bio = bio;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

}

package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle follow Operationen
 * 
 * 
 * @author Tim Weinekötter
 *
 */
public class FollowerRepository {

	private final Logger log = LoggerFactory.getLogger(FollowerRepository.class);

	/**
	 * Speichert ein Follower in der DB. INSERT.
	 * 
	 * @param f
	 */
	public void follow(Follower f) {
		log.trace("insert " + f);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("insert into follower (follower_id, followee_id) values (?,?)");
			stmt.setLong(1, f.getFollowerId());
			stmt.setLong(2, f.getFolloweeId());
			stmt.execute();

		} catch (SQLException e) {
			String msg = "SQL error while inserting follower " + f;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Löscht ein Follower in der DB. DELETE.
	 * 
	 * @param f
	 */
	public void unfollow(Follower f) {
		log.trace("delete " + f);

		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn
					.prepareStatement("delete from follower where follower_id =? and followee_id=?");
			stmt.setLong(1, f.getFollowerId());
			stmt.setLong(2, f.getFolloweeId());
			stmt.execute();

		} catch (SQLException e) {
			String msg = "SQL error while deleting follower " + f;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Überprüft ob es bereits eine Follower Instanz in der DB gibt. SELECT.
	 * 
	 * @param f
	 */
	public boolean getFollower(Follower f) {
		try (Connection conn = getConnection()) {
			PreparedStatement stmt = conn.prepareStatement(
					"select follower_id, followee_id FROM follower where follower_id=? AND followee_id=?");
			stmt.setLong(1, f.getFollowerId());
			stmt.setLong(2, f.getFolloweeId());
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				// rs is empty
				return false;
			}
			return true;

		} catch (SQLException e) {
			String msg = "SQL error while getting follower " + f;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

}

package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.before;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.PostController;
import ch.briggen.bfh.sparklist.web.PostFollowerController;
import ch.briggen.bfh.sparklist.web.PostLikeController;
import ch.briggen.bfh.sparklist.web.UserDetailController;
import ch.briggen.bfh.sparklist.web.UserListManagementRootController;
import ch.briggen.bfh.sparklist.web.UserLoginController;
import ch.briggen.bfh.sparklist.web.UserNewController;
import ch.briggen.bfh.sparklist.web.UserTimelineController;
import ch.briggen.bfh.sparklist.web.editProfileController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import ch.briggen.sparkbase.json.JsonResponseTransformer;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		for (Entry<Object, Object> property : System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(), property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {

		before("/home", (request, response) -> {
			boolean authenticated;
			UserRepository userRepo = new UserRepository();
			authenticated = userRepo.validate(request.cookie("email"), request.cookie("pw"));
			if (!authenticated) {
				response.redirect("/userlogin");
			}
		});

		before("/", (request, response) -> {
			boolean authenticated;
			UserRepository userRepo = new UserRepository();
			authenticated = userRepo.validate(request.cookie("email"), request.cookie("pw"));
			if (!authenticated) {
				response.redirect("/userlogin");
			}
			response.redirect("/home");
		});

		// Home
		get("/home", new UserListManagementRootController(), new UTF8ThymeleafTemplateEngine());

		// Edit Profile
		get("/edit-profile", new editProfileController(), new UTF8ThymeleafTemplateEngine());
		post("/edit-profile", new editProfileController(), new UTF8ThymeleafTemplateEngine());

		// User Registration
		get("/user", new UserNewController(), new UTF8ThymeleafTemplateEngine());
		post("/user", new UserNewController(), new UTF8ThymeleafTemplateEngine());

		// User Login
		get("/userlogin", new UserLoginController(), new UTF8ThymeleafTemplateEngine());
		post("userlogin", new UserLoginController(), new UTF8ThymeleafTemplateEngine());

		// User Logout
		get("/logout", (request, response) -> {
			response.removeCookie("id");
			response.removeCookie("email");
			response.removeCookie("pw");
			response.redirect("userlogin");
			return null;
		});

		// Post erfassen
		get("/post", new PostController(), new UTF8ThymeleafTemplateEngine());
		post("/post", new PostController(), new UTF8ThymeleafTemplateEngine());

		// single user
		get("single-user/:id", new UserDetailController(), new UTF8ThymeleafTemplateEngine());

		// post liken
		post("/post/like", new PostLikeController(), new JsonResponseTransformer());

		// user folgen
		post("/follow", new PostFollowerController(), new JsonResponseTransformer());
		
		// timeline
		get("/timeline", new UserTimelineController(), new UTF8ThymeleafTemplateEngine());

	}

}

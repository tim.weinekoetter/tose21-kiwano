package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import ch.briggen.bfh.sparklist.domain.FollowerRepository;
import ch.briggen.bfh.sparklist.domain.PostTlineRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für eine PostTimeline anzuzeigen
 * postTimelineTemplate
 * 
 * @author Tim Weinekötter
 */
public class UserTimelineController implements TemplateViewRoute {
	
	PostTlineRepository PostTlineRepo = new PostTlineRepository();
	UserRepository userRepo = new UserRepository();
	FollowerRepository followerRepo = new FollowerRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		if (authHelper.authResult(request)) {
			
			HashMap<String, Object> model = new HashMap<String, Object>();
			User aUser = userRepo.getByEmailPW(request.cookie("email"), request.cookie("pw"));
			
			System.out.println("ttest: "+ PostTlineRepo.getAll());

			
			model.put("posts", PostTlineRepo.getAll());
			model.put("aUser", aUser);
			return new ModelAndView(model, "postTimelineTemplate");
			
		}
		response.redirect("/userlogin");
		return null;
	}

}

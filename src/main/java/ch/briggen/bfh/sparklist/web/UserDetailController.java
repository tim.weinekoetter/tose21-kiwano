package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import ch.briggen.bfh.sparklist.domain.Follower;
import ch.briggen.bfh.sparklist.domain.FollowerRepository;
import ch.briggen.bfh.sparklist.domain.LikeRepository;
import ch.briggen.bfh.sparklist.domain.PostRepository;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für einen User und seine Posts anzuzeigen singleUserTemplate
 * singleUserTemplate
 * 
 * @author Tim Weinekötter
 */
public class UserDetailController implements TemplateViewRoute {

	UserRepository userRepo = new UserRepository();
	PostRepository postRepo = new PostRepository();
	LikeRepository likeRepo = new LikeRepository();
	FollowerRepository followerRepo = new FollowerRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		if (authHelper.authResult(request)) {

			User aUser = userRepo.getByEmailPW(request.cookie("email"), request.cookie("pw"));
			User pUser = userRepo.getById(Long.parseLong(request.params("id")));
			Follower f = new Follower(aUser.getId(), pUser.getId());
			Boolean isFollowing = followerRepo.getFollower(f);

			HashMap<String, Object> model = new HashMap<String, Object>();

			model.put("user", pUser);
			model.put("aUser", aUser);
			model.put("post", postRepo.getByUserId(pUser.getId()));
			model.put("likes", likeRepo.getLikes(pUser.getId()));
			model.put("follower", isFollowing);
			return new ModelAndView(model, "singleUserTemplate");

		}
		response.redirect("/userlogin");
		return null;
	}

}

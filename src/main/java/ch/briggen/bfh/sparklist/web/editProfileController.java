package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für Profil zu aktualisieren
 * editProfileTemplate
 * 
 * @author Tim Weinekötter
 */
public class editProfileController implements TemplateViewRoute {

	private final Logger log = LoggerFactory.getLogger(editProfileController.class);

	UserRepository userRepo = new UserRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();

		// überprüft ob User eingeloggt ist
		if (authHelper.authResult(request)) {

			// Wenn request == GET
			if (request.requestMethod().contains("GET")) {

				model.put("aUser", userRepo.getByEmailPW(request.cookie("email"), request.cookie("pw")));
				model.put("postAction", "/edit-profile");

				return new ModelAndView(model, "editProfileTemplate");
			}

			// Wenn request == POST
			long id = Long.parseLong(request.queryParams("aUser.id"));
			String firstname = request.queryParams("aUser.firstName");
			String lastname = request.queryParams("aUser.lastName");
			String bio = request.queryParams("aUser.bio");

			User u = new User(id, firstname, lastname, bio);

			log.trace("POST /edit-profile mit User " + u);

			userRepo.save(u);
			response.redirect("/home");
			return null;

		}

		// nicht eingeloggt
		response.redirect("userlogin");
		return null;

	}
}

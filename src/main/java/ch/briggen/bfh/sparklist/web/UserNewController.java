package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für User Registration 
 * userDetailTemplate
 * 
 * @author Tim Weinekötter
 */
public class UserNewController implements TemplateViewRoute {

	private UserRepository userRepo = new UserRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();

		// Wenn request == GET
		if (request.requestMethod().contains("GET")) {
			model.put("postAction", "/user");
			model.put("userDetail", new User());
			return new ModelAndView(model, "userDetailTemplate");
		}

		// Wenn request == POST
		long id = Long.parseLong(request.queryParams("userDetail.id"));
		String firstname = request.queryParams("userDetail.firstname");
		String lastname = request.queryParams("userDetail.lastname");
		String bio = request.queryParams("userDetail.bio");
		String email = request.queryParams("userDetail.email");
		String pw = request.queryParams("userDetail.pw");

		if (userRepo.getIdByEmail(email) == null) {

			User userDetail = new User(id, firstname, lastname, bio, email, pw);

			id = userRepo.insert(userDetail);
			System.out.println(id);
			response.cookie("/", "id", Long.toString(id), 10000, false);
			response.cookie("/", "email", email, 10000, false);
			response.cookie("/", "pw", pw, 10000, false);
			response.redirect("/home");

		}

		model.put("postAction", "/user");
		model.put("userDetail", new User());
		model.put("loginHint", "true");
		return new ModelAndView(model, "userDetailTemplate");

	}

}

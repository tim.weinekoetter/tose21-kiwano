package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller userlogin 
 * userloginTeplate
 * 
 * @author Tim Weinekötter
 */
public class UserLoginController implements TemplateViewRoute {

	private UserRepository userRepo = new UserRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();

		// Wenn request == GET
		if (request.requestMethod().contains("GET")) {

			model.put("postAction", "/userlogin");
			model.put("userlogin", new User());
			model.put("loginHint", "false");
			return new ModelAndView(model, "userloginTemplate");
		}

		// Wenn request == POST
		String email = request.queryParams("userlogin.email");
		String pw = request.queryParams("userlogin.pw");
		User u = new User();
		u.setEmail(email);
		u.setPw(pw);

		// Prüfung ob User Angaben stimmen
		if (userRepo.validate(email, pw)) {
			String id = Long.toString(userRepo.getIdByEmail(email));
//				String id = Long.toString(userRepo.getByEmail(email).iterator().next().getId());
			response.cookie("/", "id", id, 10000, false);
			response.cookie("/", "email", email, 10000, false);
			response.cookie("/", "pw", pw, 10000, false);
			response.redirect("/home");
		}

		// User Angaben stimmen nicht
		model.put("postAction", "/userlogin");
		model.put("userlogin", new User());
		model.put("loginHint", "true");
		return new ModelAndView(model, "userloginTemplate");

	}

}

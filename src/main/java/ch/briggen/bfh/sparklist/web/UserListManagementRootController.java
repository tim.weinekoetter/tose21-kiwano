package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für User anzuzeigen 
 * userListTemplate
 * 
 * @author Tim Weinekötter
 */
public class UserListManagementRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(UserListManagementRootController.class);

	UserRepository repository = new UserRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("user", repository.getAll());
		model.put("aUser", repository.getByEmailPW(request.cookie("email"), request.cookie("pw")));

		System.out.println("Cookie: " + request.cookie("id"));
		System.out.println("Cookie: " + request.cookie("pw"));

		return new ModelAndView(model, "userListTemplate");
	}

}

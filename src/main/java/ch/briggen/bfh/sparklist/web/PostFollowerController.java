package ch.briggen.bfh.sparklist.web;

import com.google.gson.Gson;
import ch.briggen.bfh.sparklist.domain.Follower;
import ch.briggen.bfh.sparklist.domain.FollowerRepository;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Controller für User zu folgens
 * Hört auf Ajax Post
 *
 * @author Tim Weinekötter
 */
public class PostFollowerController implements Route {

	private final Gson parser = new Gson();
	private FollowerRepository followerRepo = new FollowerRepository();

	@Override
	public Object handle(Request request, Response response) throws Exception {
		final String body = request.body();
		Follower f = parser.fromJson(body, Follower.class);

		// checks if a follow entry already exists
		if (followerRepo.getFollower(f)) {

			// true --> entry exists --> Delete existing Follow instance
			followerRepo.unfollow(f);
			response.status(201);
			return null;
		}
		// false -> no entry exists
		followerRepo.follow(f);
		response.status(201);
		return null;

	}

}

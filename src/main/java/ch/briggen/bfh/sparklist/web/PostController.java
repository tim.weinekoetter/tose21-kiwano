package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import ch.briggen.bfh.sparklist.domain.Post;
import ch.briggen.bfh.sparklist.domain.PostRepository;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für Posts zu erstellen 
 * newPostTemplate
 * 
 * @author Tim Weinekötter
 */
public class PostController implements TemplateViewRoute {

	UserRepository userRepo = new UserRepository();
	PostRepository postRepo = new PostRepository();

	public ModelAndView handle(Request request, Response response) throws Exception {

		if (authHelper.authResult(request)) {

			// Wenn request == GET
			if (request.requestMethod().contains("GET")) {
				System.out.println("its GET method");

				HashMap<String, Object> model = new HashMap<String, Object>();
				model.put("post", new Post());
				model.put("aUser", userRepo.getByEmailPW(request.cookie("email"), request.cookie("pw")));
				model.put("postAction", "/post");

				return new ModelAndView(model, "newPostTemplate");
			}

			// Wenn request == POST
			System.out.println("its POST method");
			long user_id = Long.parseLong(request.queryParams("aUser.id"));
			String summary = request.queryParams("post.summary");
			String content = request.queryParams("post.content");
			Post p = new Post(user_id, summary, content);
			postRepo.insert(p);

			response.redirect("/home");
			return null;
		}

		response.redirect("userlogin");
		return null;

	}

}

package ch.briggen.bfh.sparklist.web;

import spark.Request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UserRepository;

/**
 * authHelper Klasse. überprüft ob User eingloggt ist
 * 
 * @author Tim Weinekötter
 */
public class authHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(authHelper.class);

	public static boolean authResult(Request request) {

		UserRepository userRepo = new UserRepository();
		boolean authenticated = userRepo.validate(request.cookie("email"), request.cookie("pw"));

		return authenticated;

	}

}

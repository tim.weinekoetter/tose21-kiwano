package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import ch.briggen.bfh.sparklist.domain.Like;
import ch.briggen.bfh.sparklist.domain.LikeRepository;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Controller für Posts zu liken Wenn Post bereits geliked wurde, wird Like
 * gelöscht, ansonsten wird eine neue Like Instanz in der DB erstellt. 
 * Hört auf Ajax Post
 * 
 * @author Tim Weinekötter
 */
public class PostLikeController implements Route {

	private final Logger log = LoggerFactory.getLogger(PostLikeController.class);
	private final Gson parser = new Gson();
	private LikeRepository likeRepo = new LikeRepository();

	@Override
	public Object handle(Request request, Response response) throws Exception {
		final String body = request.body();
		log.debug("POST /post/like " + body);
		Like l = parser.fromJson(body, Like.class);

		// checks if an like entry already exists
		if (likeRepo.getLike(l)) {

			// true -> no entry exists --> Insert Like instance
			likeRepo.like(l);
			response.status(201);
			return null;
		}

		// false --> entry exists --> Delete existing Like instance
		likeRepo.unlike(l);
		response.status(201);
		return null;
	}

}

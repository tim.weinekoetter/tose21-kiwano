package ch.briggen.sparkbase.json;

public class JsonErrorResponse {
	private final int status;
	private final String message;

	public JsonErrorResponse(int httpStatusCode, String message)
	{
		this.status=httpStatusCode;
		this.message=message;
	}
	
	@Override
	public String toString() {
		
		return "JsonErrorResponse: " + status + " - " + message;
	}
}
